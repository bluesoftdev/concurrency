# Endurance Concurrency #

Some classes to facilitate concurrent programming in Java.

## Usage ##

To use in your project, just depend on the latest version.  There is no released version yet, so the current version is 1.0.0-SNAPSHOT.

Gradle:

```
#!groovy

dependencies {
  ...
  compile 'com.bluesoftdev.endurance:concurrency:1.0.0-SNAPSHOT'
}
```

Maven:
```
#!xml

<dependency>
  <groupId>com.bluesoftdev.endurance</groupId>
  <artifactId>concurrency</artifactId>
  <version>1.0.0-SNAPSHOT</version>
</dependency>
```

## Contributions ##

Pull requests are encouraged.  Please fork and make your changes.  We use the [Git Flow](http://nvie.com/posts/a-successful-git-branching-model/) to manage the source code life cycle, so please make any changes to 'develop'.