/*
 * Copyright 2014-2015 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.concurrent;

import java.util.Timer;
import java.util.concurrent.Executors;

/**
 * Default executors for common tasks.
 */
public class MoreExecutors {

  /**
   * A good pool for executing non-blocking work on.  Used as the default executor for many of the library functions
   * in Endurance concurrency.
   *
   * @see Executors#newWorkStealingPool()
   */
  public static final ListenableExecutorService NON_BLOCKING = new ListenableExecutorServiceDecorator(Executors.newWorkStealingPool());

  /**
   * An open ended thread pool that will start new threads as needed.  Careful with this one, OOM Exceptions can be
   * thrown if operating system thread limits are reached.
   *
   * @see Executors#newCachedThreadPool()
   */
  public static final ListenableExecutorService BLOCKING = new ListenableExecutorServiceDecorator(Executors.newCachedThreadPool());

  /**
   * A scheduled task service with one thread.  Tasks should not tie up the thread with work.  If any work needs to be
   * done by the task, dispatch it to the appropriate thread pool.
   *
   * @see Executors#newSingleThreadScheduledExecutor()
   */
  public static final ScheduledListenableExecutorService SCHEDULED = new ListenableScheduledThreadPoolExecutor(1);

  public static final Timer TIMEOUT_TIMER = new Timer("Concurrency Timeout Timer",true);
}
