/*
 * Copyright 2014-2015 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.concurrent;

/**
 * A {@link ListenableFuture} that can be set in the future or given a value at creation.
 * <p>
 * @author Dana P'Simer &lt;danap@bluesoftdev.com&gt;
 */
public class SettableListenableFuture<T> extends AbstractListenableFuture<T> {

  private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger( SettableListenableFuture.class );

  /**
   * New settable future in a settable state.
   */
  public SettableListenableFuture() {
    super();
    running();
  }

  /**
   * New settable future in a set state.
   * <p>
   * @param value the value.
   */
  public SettableListenableFuture( T value ) {
    running();
    success( value );
  }

  /**
   * new settable future in a failed state.
   * <p>
   * @param failure the failure.
   */
  public SettableListenableFuture( Throwable failure ) {
    running();
    failed( failure );
  }

  @Override
  public boolean cancel( boolean mayInterruptIfRunning ) {
    synchronized ( this ) {
      if ( !isDone() ) {
        if ( mayInterruptIfRunning ) {
          interrupting();
          interrupted();
          return true;
        }
      }
      return false;
    }
  }

  /**
   * Set the value.
   * <p>
   * @param value the value.
   */
  public void set( T value ) {
    synchronized ( this ) {
      success( value );
    }
  }

  /**
   * Set the failure.
   * <p>
   * @param t the failure.
   */
  public void setException( Throwable t ) {
    synchronized ( this ) {
      failed( t );
    }
  }
}
