/*
 * Copyright 2014-2015 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.concurrent;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * An extension of {@link ExecutorService} that provides {@link ListenableFuture}s.
 * <p>
 * @author Dana H. P'Simer &lt;danap@bluesoftdev.com&gt;
 */
public interface ListenableExecutorService extends ExecutorService {

  /**
   * {@inheritDoc}
   */
  @Override
  default <T> List<Future<T>> invokeAll( Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit ) throws InterruptedException {
    List<Future<T>> futures = new ArrayList<>( tasks.size() );
    tasks.stream().forEach( (task) -> {
      futures.add( submit( task ) );
    } );
    return futures;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  default <T> List<Future<T>> invokeAll( Collection<? extends Callable<T>> tasks ) throws InterruptedException {
    return invokeAll( tasks, -1L, null );
  }

  /**
   * {@inheritDoc}
   */
  @Override
  ListenableFuture<?> submit( Runnable task );

  /**
   * {@inheritDoc}
   */
  @Override
  <T> ListenableFuture<T> submit( Runnable task, T result );

  /**
   * {@inheritDoc}
   */
  @Override
  <T> ListenableFuture<T> submit( Callable<T> task );

}
