/*
 * Copyright 2014-2015 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.concurrent;

import java.util.concurrent.CancellationException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;

/**
 * A base class for implementing {@link ListenableFuture}s.
 * <p>
 * @param <T> the type of the data held by the future.
 * <p>
 * @author Dana H. P'Simer &lt;danap@bluesoftdev.com&gt;
 */
public abstract class AbstractListenableFuture<T> implements ListenableFuture<T> {

  private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger( AbstractListenableFuture.class );

  enum State {

    NEW( false, false ),
    RUNNING( false, false ),
    SUCESS( true, false ),
    FAILED( true, false ),
    INTERRUPTING( false, false ),
    INTERRUPTED( true, true ),
    CANCELLED( true, true );

    private final boolean terminal;
    private final boolean cancelled;

    State( boolean terminal, boolean cancelled ) {
      this.terminal = terminal;
      this.cancelled = cancelled;
    }

    public boolean isTerminal() {
      return terminal;
    }

    public boolean isCancelled() {
      return cancelled;
    }

  }

  private final AtomicReference<State> state = new AtomicReference<>( State.NEW );
  private T value = null;
  private Throwable cause = null;
  private final CountDownLatch completionLatch = new CountDownLatch( 1 );
  private final ExecutionList listeners = new ExecutionList();

  protected final void cancelled( boolean mayInterrupt ) {
    if ( !state.compareAndSet( State.NEW, State.CANCELLED ) ) {
      if ( mayInterrupt ) {
        interrupting();
        interrupt();
        interrupted();
      } else {
        throw new IllegalStateException( "cannot cancel, future task was already running.  Use cancel(true)." );
      }
    } else {
      this.cause = new CancellationException();
      done();
    }
  }

  protected final void interrupting() {
    if ( !state.compareAndSet( State.RUNNING, State.INTERRUPTING ) ) {
      throw new IllegalStateException( "cannot interrupt, the future is not running." );
    }
  }

  protected final void interrupted() {
    if ( !state.compareAndSet( State.INTERRUPTING, State.INTERRUPTED ) ) {
      throw new IllegalStateException( "state was not INTERRUPTING" );
    }
    this.cause = new InterruptedException();
    done();
  }

  protected final void running() {
    if ( !state.compareAndSet( State.NEW, State.RUNNING ) ) {
      throw new IllegalStateException( "state was not NEW" );
    }
  }

  protected final void success( T value ) {
    if ( !state.compareAndSet( State.RUNNING, State.SUCESS ) ) {
      throw new IllegalStateException( "state was not RUNNING" );
    }
    this.value = value;
    done();
  }

  protected final void failed( Throwable cause ) {
    if ( !state.compareAndSet( State.RUNNING, State.FAILED ) ) {
      throw new IllegalStateException( "state was not RUNNING" );
    }
    this.cause = cause;
    done();
  }

  /**
   * Called when a future moves to a terminal state. May be overridden to add functionality to when the future moves to a terminal state.
   */
  protected void done() {
    completionLatch.countDown();
    listeners.execute();
  }

  /**
   * Function that can interrupt a running task.
   */
  protected void interrupt() {
    // NO OP - OVERRIDE to take some action to interrupt the task.
  }

  @Override
  public boolean isCancelled() {
    return state.get().isCancelled();
  }

  @Override
  public boolean isDone() {
    return state.get().isTerminal();
  }

  public boolean isRunning() {
    return state.get() == State.RUNNING;
  }

  @Override
  public T get() throws InterruptedException, ExecutionException {
    completionLatch.await();
    State s = state.get();
    assert s.isTerminal();
    switch ( s ) {
      case CANCELLED:
        throw (CancellationException)this.cause;
      case INTERRUPTED:
        throw (InterruptedException)this.cause;
      case FAILED:
        throw new ExecutionException( cause );
      case SUCESS:
        return value;
      default:
        throw new IllegalStateException( "future was not done." );
    }
  }

  @Override
  public T get( long timeout, TimeUnit unit ) throws InterruptedException, ExecutionException, TimeoutException {
    completionLatch.await( timeout, unit );
    return get();
  }

  @Override
  public void addListener( Runnable listener, Executor executor ) {
    listeners.add( listener, executor );
  }

  @Override
  public void addListener( FutureListener<T> listener, Executor executor ) {
    listeners.add( () -> listener.onCompletion( this ), executor );
  }
}
