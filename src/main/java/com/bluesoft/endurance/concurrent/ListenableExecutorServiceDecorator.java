/*
 * Copyright 2014-2015 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.concurrent;

import java.util.List;
import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.TimeUnit;

/**
 * A decorator for an {@link ExecutorService} that provides {@link ListenableFuture}s
 * <p>
 * @author Dana H. P'Simer &lt;danap@bluesoftdev.com&gt;
 */
public class ListenableExecutorServiceDecorator extends AbstractExecutorService implements ListenableExecutorService {

  private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger( ListenableExecutorServiceDecorator.class );
  private final ExecutorService delegate;

  public ListenableExecutorServiceDecorator( ExecutorService delegate ) {
    this.delegate = delegate;
  }

  @Override
  public <T> ListenableFuture<T> submit( Callable<T> task ) {
    return (ListenableFuture<T>)super.submit( task );
  }

  @Override
  public <T> ListenableFuture<T> submit( Runnable task, T result ) {
    return (ListenableFuture<T>)super.submit( task, result );
  }

  @Override
  public ListenableFuture<?> submit( Runnable task ) {
    return (ListenableFuture<?>)super.submit( task );
  }

  @Override
  protected <T> RunnableFuture<T> newTaskFor( Callable<T> callable ) {
    return new ListenableFutureTask<>( callable );
  }

  @Override
  protected <T> RunnableFuture<T> newTaskFor( Runnable runnable, T value ) {
    return new ListenableFutureTask<>( runnable, value );
  }

  @Override
  public boolean awaitTermination( long timeout, TimeUnit unit ) throws InterruptedException {
    return delegate.awaitTermination( timeout, unit );
  }

  @Override
  public boolean isShutdown() {
    return delegate.isShutdown();
  }

  @Override
  public boolean isTerminated() {
    return delegate.isTerminated();
  }

  @Override
  public void shutdown() {
    delegate.shutdown();
  }

  @Override
  public List<Runnable> shutdownNow() {
    return delegate.shutdownNow();
  }

  @Override
  public void execute( Runnable command ) {
    delegate.execute( command );
  }

}
