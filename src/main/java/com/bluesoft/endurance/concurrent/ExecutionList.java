/*
 * Copyright 2014-2015 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.concurrent;

import java.util.concurrent.Executor;

/**
 * Manages a list of {@link Runnable}s that will be executed at some future time. Once the {@link #execute()} method has been called, any listeners
 * that are added will be submitted to the provided executor immediately.
 * <p>
 * @param <T> the type of {@link Runnable} that will be executed.
 * <p>
 * @author Dana H. P'Simer &lt;danap@bluesoftdev.com&gt;
 */
public class ExecutionList<T extends Runnable> {

  private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger( ExecutionList.class );

  public class Runner {

    Runnable r;
    Executor e;
    Runner next;

    public Runner( Runnable r, Executor e, Runner next ) {
      this.r = r;
      this.e = e;
      this.next = next;
    }

  }
  private boolean executed = false;
  private Runner runners = null;

  /**
   * Execute the listeners.
   */
  public void execute() {
    Runner list = null;
    synchronized ( this ) {
      if ( executed ) {
        return;
      }
      executed = true;
      list = runners;
      runners = null;
    }
    Runner reversed = null;
    while (list != null) {
      Runner tmp = list;
      list = list.next;
      tmp.next = reversed;
      reversed = tmp;
    }
    while (reversed != null) {
      executeRunner( reversed.r, reversed.e );
      reversed = reversed.next;
    }
  }

  public void add( Runnable r, Executor e ) {
    synchronized ( this ) {
      if ( !executed ) {
        runners = new Runner( r, e, runners );
        return;
      }
    }
    executeRunner( r, e );
  }

  private void executeRunner( Runnable r, Executor e ) {
    try {
      e.execute( r );
    } catch ( RuntimeException ex ) {
      LOG.error( "failed to execute runner.", ex );
    }
  }
}
