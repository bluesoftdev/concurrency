/*
 * Copyright 2014-2015 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.concurrent;

import java.util.concurrent.*;

/**
 * Created by danap on 9/2/15.
 */
public class ListenableScheduledThreadPoolExecutor extends ScheduledThreadPoolExecutor implements ScheduledListenableExecutorService {
  public ListenableScheduledThreadPoolExecutor(int corePoolSize) {
    super(corePoolSize);
  }

  public ListenableScheduledThreadPoolExecutor(int corePoolSize, ThreadFactory threadFactory) {
    super(corePoolSize, threadFactory);
  }

  public ListenableScheduledThreadPoolExecutor(int corePoolSize, RejectedExecutionHandler handler) {
    super(corePoolSize, handler);
  }

  public ListenableScheduledThreadPoolExecutor(int corePoolSize, ThreadFactory threadFactory, RejectedExecutionHandler handler) {
    super(corePoolSize, threadFactory, handler);
  }

  @Override
  public <T> ListenableFuture<T> submit(Callable<T> task) {
    return (ListenableFuture<T>)super.submit(task);
  }

  @Override
  public ListenableFuture<?> submit(Runnable task) {
    return (ListenableFuture)super.submit(task);
  }

  @Override
  public <T> ListenableFuture<T> submit(Runnable task, T result) {
    return (ListenableFuture<T>)super.submit(task, result);
  }

  @Override
  public ScheduledListenableFuture<?> scheduleWithFixedDelay(Runnable command, long initialDelay, long delay, TimeUnit unit) {
    return (ScheduledListenableFuture<?>)super.scheduleWithFixedDelay(command, initialDelay, delay, unit);
  }

  @Override
  public ScheduledListenableFuture<?> scheduleAtFixedRate(Runnable command, long initialDelay, long period, TimeUnit unit) {
    return (ScheduledListenableFuture<?>)super.scheduleAtFixedRate(command, initialDelay, period, unit);
  }

  @Override
  public <V> ScheduledListenableFuture<V> schedule(Callable<V> callable, long delay, TimeUnit unit) {
    return (ScheduledListenableFuture<V>)super.schedule(callable, delay, unit);
  }

  @Override
  public ScheduledListenableFuture<?> schedule(Runnable command, long delay, TimeUnit unit) {
    return (ScheduledListenableFuture<?>)super.schedule(command, delay, unit);
  }

  @Override
  protected <V> RunnableScheduledFuture<V> decorateTask(Runnable runnable, RunnableScheduledFuture<V> task) {
    return new ScheduledListenableFutureTask<>(runnable, task);
  }

  @Override
  protected <V> RunnableScheduledFuture<V> decorateTask(Callable<V> callable, RunnableScheduledFuture<V> task) {
    return  new ScheduledListenableFutureTask<>(callable, task);
  }
}
