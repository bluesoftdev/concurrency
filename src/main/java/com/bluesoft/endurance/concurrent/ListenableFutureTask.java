/*
 * Copyright 2014-2015 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.concurrent;

import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;

/**
 * Defines a {@link ListenableFuture} implementation that extends {@link FutureTask}.
 * <p>
 * @param <T> The return type of the future.
 * <p>
 * @author Dana H. P'Simer &lt;danap@bluesoftdev.com&gt;
 */
public class ListenableFutureTask<T> extends FutureTask<T> implements ListenableFuture<T> {

  @SuppressWarnings("unused")
  private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger( ListenableFutureTask.class );

  private final ExecutionList<Runnable> listeners = new ExecutionList<Runnable>();

  /**
   * {@inheritDoc}
   */
  public ListenableFutureTask( Callable<T> callable ) {
    super( callable );
  }

  /**
   * {@inheritDoc}
   */
  public ListenableFutureTask( Runnable runnable, T result ) {
    super( runnable, result );
  }

  @Override
  protected void done() {
    listeners.execute();
  }

  @Override
  public void addListener( FutureListener<T> listener, Executor executor ) {
    listeners.add( () -> listener.onCompletion( this ), executor );
  }
}
