/*
 * Copyright 2014-2015 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.concurrent;

import java.util.concurrent.*;

/**
 * Created by danap on 9/2/15.
 */
public class ScheduledListenableFutureTask<T> extends ListenableFutureTask<T> implements RunnableScheduledFuture<T>, ScheduledListenableFuture<T> {

  private RunnableScheduledFuture<T> delegate;

  public ScheduledListenableFutureTask(Runnable runnable,RunnableScheduledFuture<T> task) {
    super(runnable,null);
    delegate = task;
  }

  public ScheduledListenableFutureTask(Callable<T> callable,RunnableScheduledFuture<T> task) {
    super(callable);
    delegate = task;
  }

  @Override
  public long getDelay(TimeUnit unit) {
    return delegate.getDelay(unit);
  }

  @Override
  public int compareTo(Delayed o) {
    return delegate.compareTo(o);
  }

  @Override
  public boolean isPeriodic() {
    return delegate.isPeriodic();
  }

  @Override
  public void run() {
    try {
      delegate.run();
    } finally {
      try {
        set(delegate.get());
      } catch (ExecutionException ex) {
        setException(ex.getCause());
      } catch (Throwable ex) {
        setException(ex);
      } finally {
        done();
      }
    }
  }
}
