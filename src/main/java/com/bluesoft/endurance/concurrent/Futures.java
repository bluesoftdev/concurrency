/*
 * Copyright 2014-2015 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.concurrent;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;

/**
 * Some utility functions for manipulating futures.
 * @author danap
 */
public class Futures {
  public static <T> ListenableFuture<T> successful(T t) {
    return new SettableListenableFuture<>(t);
  }

  public static <T> ListenableFuture<T> failure(Throwable ex) {
    return new SettableListenableFuture<>(ex);
  }

  public static <T> ListenableFuture<List<T>> list(Collection<? extends ListenableFuture<? extends T>> futures, Executor executor) {
    SettableListenableFuture<List<T>> futureList = new SettableListenableFuture<>();
    List<T> accumulator = new ArrayList<>(futures.size());
    for (ListenableFuture<? extends T> f : futures) {
      f.addListener(() -> {
        if (futureList.isDone()) {
          return;
        }
        try {
          T t = f.getUninterruptibly();
          synchronized (accumulator) {
            accumulator.add(t);
          }
          if (accumulator.size() == futures.size()) {
            synchronized (futureList) {
              if (!futureList.isDone()) {
                futureList.set(accumulator);
              }
            }
          }
        } catch (Throwable ex) {
          // unwrap any ExecutionExceptions.
          if (ex instanceof ExecutionException) {
            ex = ex.getCause();
          }
          synchronized (futureList) {
            if (!futureList.isDone()) {
              futureList.setException(ex);
            }
          }
        }
      },executor);
    }
    return futureList;
  }
}
