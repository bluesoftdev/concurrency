/*
 * Copyright 2014-2015 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.concurrent;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.*;

/**
 * An {@link ExecutorService} that runs the submitted tasks on the current thread.
 * <p>
 *
 * @author Dana H. P'Simer &lt;danap@bluesoftdev.com&gt;
 */
public class SameThreadExecutorService implements ListenableExecutorService {

  private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(SameThreadExecutorService.class);

  public static final SameThreadExecutorService INSTANCE = new SameThreadExecutorService();

  boolean shutdown = false;

  @Override
  public void shutdown() {
    shutdown = true;
  }

  @Override
  public List<Runnable> shutdownNow() {
    shutdown = true;
    return Collections.EMPTY_LIST;
  }

  @Override
  public boolean isShutdown() {
    return shutdown;
  }

  @Override
  public boolean isTerminated() {
    return shutdown;
  }

  @Override
  public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
    return true;
  }

  @Override
  public void execute(Runnable command) {
    command.run();
  }

  @Override
  public ListenableFuture<?> submit(Runnable task) {
    return submit(task, null);
  }

  @Override
  public <T> ListenableFuture<T> submit(Runnable task, T result) {
    return submit(() -> {
      task.run();
      return result;
    });
  }

  @Override
  public <T> ListenableFuture<T> submit(Callable<T> task) {
    SettableListenableFuture<T> future = new SettableListenableFuture<>();
    try {
      future.set(task.call());
    } catch (ExecutionException ex) {
      future.setException(ex.getCause());
    } catch (Exception ex) {
      future.setException(ex);
    }
    return future;
  }

  @Override
  public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks) throws InterruptedException {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit)
          throws InterruptedException {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public <T> T invokeAny(Collection<? extends Callable<T>> tasks) throws InterruptedException, ExecutionException {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public <T> T invokeAny(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit)
          throws InterruptedException, ExecutionException, TimeoutException {
    throw new UnsupportedOperationException("Not supported yet.");
  }
}
