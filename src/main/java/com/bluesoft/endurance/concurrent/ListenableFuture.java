/*
 * Copyright 2014-2015 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.concurrent;

import java.util.TimerTask;
import java.util.concurrent.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;


/**
 * A {@link Future} that allows for the listeners to be registered and called when the result is ready.
 *
 * @param <T> the type of data held by the Future.
 * @author Dana H. P'Simer &lt;danap@bluesoftdev.com&gt;
 */
public interface ListenableFuture<T> extends Future<T> {

  /**
   * Add a listener that will run on the same thread that completes the future.  This should be a non-blocking function
   * that does not execute significant work.  If your listener must block or do significant calculation, please consider
   * using {@link #addListener(Runnable, Executor)} to specify an executor on which the action is to take place.
   *
   * @param listener the listener.
   */
  default void addListener(Runnable listener) {
    addListener(listener, MoreExecutors.NON_BLOCKING);
  }

  /**
   * Add a listener that will run on the same thread completes the future.  This should be a non-blocking function
   * that does not execute significant work.  If your listener must block or do significant calculation, please consider
   * using {@link #addListener(FutureListener, Executor)} to specify an executor on which the action is to take place.
   *
   * @param listener the listener.
   */
  default void addListener(FutureListener<T> listener) {
    addListener(listener, MoreExecutors.NON_BLOCKING);
  }

  /**
   * Add a listener that will be run on the given executor when this future completes.
   *
   * @param listener the listener
   * @param executor the executor
   */
  default void addListener(Runnable listener, Executor executor) {
    addListener((ListenableFuture<T> lf) -> listener.run(), executor);
  }

  /**
   * Add a listener that will be run on the given executor when this future completes.
   *
   * @param listener the listener
   * @param executor the executor
   */
  void addListener(FutureListener<T> listener, Executor executor);

  /**
   * Get the value uninterruptably.
   *
   * @return the value of the future.
   * @throws ExecutionException when the future is in a failed state.
   */
  default T getUninterruptibly() throws ExecutionException {
    boolean interrupted = false;
    try {
      while (true) {
        try {
          return get();
        } catch (InterruptedException e) {
          interrupted = true;
        }
      }
    } finally {
      if (interrupted) {
        Thread.currentThread().interrupt();
      }
    }
  }

  /**
   * Get the value uninterruptably with a timeout.
   *
   * @param timeout the timeout.
   * @param unit    the timeunit the timeout is expressed in.
   * @return the value of the future.
   * @throws ExecutionException when the future is in a failed state.
   * @throws TimeoutException   when the future does complete in the alotted time.
   */
  default T getUninterruptibly(long timeout, TimeUnit unit) throws ExecutionException, TimeoutException {
    boolean interrupted = false;
    try {
      long remainingNanos = unit.toNanos(timeout);
      long end = System.nanoTime() + remainingNanos;

      while (true) {
        try {
          // Future treats negative timeouts just like zero.
          return get(remainingNanos, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
          interrupted = true;
          if (isCancelled()) {
            break;
          }
          remainingNanos = end - System.nanoTime();
        }
      }
    } finally {
      if (interrupted) {
        Thread.currentThread().interrupt();
      }
    }
    return null;
  }

  /**
   * maps the value returned by this future to a value of another type using the mapping function provided.
   *
   * @param mapper   the mapping function.  This should be a non-blocking function.  If a blocking operation needs to be
   *                 performed, consider {@link #flatMap(Function)}
   * @param executor the executor to use when executing the mapping function.
   * @param <R>      the type of the result of the mapping function
   * @return a future that will return the mapped result when it is ready.
   */
  default <R> ListenableFuture<R> map(Function<T, R> mapper, Executor executor) {
    SettableListenableFuture<R> mapped = new SettableListenableFuture<>();
    addListener(() -> {
      try {
        mapped.set(mapper.apply(getUninterruptibly()));
      } catch (ExecutionException ex) {
        mapped.setException(ex.getCause());
      } catch (Throwable ex) {
        mapped.setException(ex);
      }
    }, executor);
    return mapped;
  }

  /**
   * maps the value returned by this future to a value of another type using the mapping function provided.
   * <p>
   * NOTE: Uses the {@link MoreExecutors#NON_BLOCKING} executor service.  Therefore, the code should not block.  Provide
   * another executor if a blocking call must be used.
   *
   * @param mapper the mapping function.  This should be a non-blocking function.  If a blocking operation needs to be
   *               performed, consider {@link #flatMap(Function)}
   * @param <R>    the type of the result of the mapping function
   * @return a future that will return the mapped result when it is ready.
   */
  default <R> ListenableFuture<R> map(Function<T, R> mapper) {
    return map(mapper, MoreExecutors.NON_BLOCKING);
  }

  /**
   * Allows the chaining of asynchronous actions.  Accepts a function that returns a future in response to a successful
   * result of this future.
   *
   * @param mapper   the mapping function
   * @param executor the executor to use when executing the mapping function.
   * @param <R>      the type of the mapped value.
   * @return a future that will return the mapped value at a later time.
   */
  default <R> ListenableFuture<R> flatMap(Function<T, ListenableFuture<R>> mapper, Executor executor) {
    SettableListenableFuture<R> mapped = new SettableListenableFuture<>();
    addListener(f -> {
      try {
        ListenableFuture<R> rFuture = mapper.apply(f.getUninterruptibly());
        rFuture.addListener(() -> {
          try {
            mapped.set(rFuture.getUninterruptibly());
          } catch (ExecutionException ex) {
            mapped.setException(ex.getCause());
          } catch (Throwable ex) {
            mapped.setException(ex);
          }
        });
      } catch (ExecutionException ex) {
        mapped.setException(ex.getCause());
      } catch (Throwable ex) {
        mapped.setException(ex);
      }
    }, executor);
    return mapped;
  }

  /**
   * Allows the chaining of asynchronous actions.  Accepts a function that returns a future in response to a successful
   * result of this future.
   * <p>
   * NOTE: Uses the {@link MoreExecutors#NON_BLOCKING} executor service.  Therefore, the code should not block.  Provide
   * another executor if a blocking call must be used.
   *
   * @param mapper the mapping function
   * @param <R>    the type of the mapped value.
   * @return a future that will return the mapped value at a later time.
   */
  default <R> ListenableFuture<R> flatMap(Function<T, ListenableFuture<R>> mapper) {
    return flatMap(mapper, MoreExecutors.NON_BLOCKING);
  }

  /**
   * Sets up a timer that will force the returned future to complete with a TimeoutException when it elapses before
   * the future is completed.  There is a chance for a race condition that can cause a result that comes in right after
   * the timeout triggers to be discarded.  In this case, the onDiscardedResult or onDiscardedException are called as
   * appropriate.
   *
   * @param timeout              the time to wait for the completion of this future.  if this value is 0 or less, then
   *                             no timeout will be applied.
   * @param timeUnit             the timeUnit that the timeout value is expressed in.
   * @param onDiscardedResult    the consumer that will receive any discarded results.
   * @param onDiscardedException the consumer that will receive any discarded exceptions.
   * @param onTimeout            a predicate that will be called when the timeout is reached, returning true will result
   *                             in the future completing with a TimeoutException, returning false will wait for the same
   *                             timeout interval again.
   * @return a future that will either produce a result, propagate an exception or receive a TimeoutException when the
   * timeout expires.
   */
  default ListenableFuture<T> timeout(long timeout, TimeUnit timeUnit,
                                      Consumer<T> onDiscardedResult,
                                      Consumer<Throwable> onDiscardedException,
                                      Predicate<ListenableFuture<T>> onTimeout) {
    // Don't inject timeout logic if the timeout is 0 or less.
    if (timeout <= 0) {
      return this;
    }
    ListenableFuture<T> thisFuture = this;
    TimeoutException toEx = new TimeoutException();
    SettableListenableFuture<T> settable = new SettableListenableFuture<>();
    long timeoutMS = TimeUnit.MILLISECONDS.convert(timeout, timeUnit);
    TimerTask whenTimeout = new TimerTask() {
      @Override
      public void run() {
        if (onTimeout.test(thisFuture)) {
          if (thisFuture.cancel(true)) {
            synchronized (settable) {
              if (!settable.isDone()) {
                settable.setException(toEx);
              }
            }
          }
        } else {
          MoreExecutors.TIMEOUT_TIMER.schedule(this, timeoutMS);
        }
      }
    };
    MoreExecutors.TIMEOUT_TIMER.schedule(whenTimeout, timeoutMS);
    addListener(f -> {
      whenTimeout.cancel();
      boolean wasDone;
      synchronized (settable) {
        wasDone = settable.isDone();
        if (!wasDone) {
          try {
            settable.set(f.getUninterruptibly());
          } catch (ExecutionException ex) {
            settable.setException(ex.getCause());
          } catch (Throwable ex) {
            settable.setException(ex);
          }
        }
      }
      // so that calls to the discarded result/exception methods happen outside the synchronized block.
      if (wasDone) {
        try {
          onDiscardedResult.accept(f.getUninterruptibly());
        } catch (ExecutionException ex) {
          onDiscardedException.accept(ex.getCause());
        } catch (Throwable ex) {
          onDiscardedException.accept(ex);
        }
      }
    });
    return settable;
  }

  /**
   * Sets up a timer that will force the returned future to complete with a TimeoutException when it elapses before
   * the future is completed.  There is a chance for a race condition that can cause a result that comes in right after
   * the timeout triggers to be discarded.  In this case, the onDiscardedResult or onDiscardedException are called as
   * appropriate.
   *
   * @param timeout              the time to wait for the completion of this future.  if this value is 0 or less, then
   *                             no timeout will be applied.
   * @param timeUnit             the timeUnit that the timeout value is expressed in.
   * @param onDiscardedResult    the consumer that will receive any discarded results.
   * @param onDiscardedException the consumer that will receive any discarded exceptions.
   * @return a future that will either produce a result, propagate an exception or receive a TimeoutException when the
   * timeout expires.
   */
  default ListenableFuture<T> timeout(long timeout, TimeUnit timeUnit, Consumer<T> onDiscardedResult, Consumer<Throwable> onDiscardedException) {
    return timeout(timeout, timeUnit, onDiscardedResult, onDiscardedException, f -> true);
  }

  /**
   * Same as {@link #timeout(long, TimeUnit, Consumer, Consumer)} but with the onDiscardedException stubbed to a NoOp.
   *
   * @param timeout           the time to wait for the completion of this future.
   * @param timeUnit          the timeUnit that the timeout value is expressed in.
   * @param onDiscardedResult the consumer that will receive any discarded results.
   * @return a future that will either produce a result, propagate an exception or receive a TimeoutException when the
   * timeout expires.
   * @see #timeout(long, TimeUnit, Consumer, Consumer)
   */
  default ListenableFuture<T> timeout(long timeout, TimeUnit timeUnit, Consumer<T> onDiscardedResult) {
    return timeout(timeout, timeUnit, onDiscardedResult, e -> {
    });
  }

  /**
   * Same as {@link #timeout(long, TimeUnit, Consumer, Consumer)} with both discarded consumers stupped as NoOps.
   *
   * @param timeout  the time to wait for the completion of this future.
   * @param timeUnit the timeUnit that the timeout value is expressed in.
   * @return a future that will either produce a result, propagate an exception or receive a TimeoutException when the
   * timeout expires.
   * @see #timeout(long, TimeUnit, Consumer, Consumer)
   */
  default ListenableFuture<T> timeout(long timeout, TimeUnit timeUnit) {
    return timeout(timeout, timeUnit, t -> {
    }, e -> {
    });
  }

  /**
   * The consumer is called when the future completes with an exception condition.
   *
   * @param onEx     the consumer that will be called when the future completes with an exception.
   * @param executor the executor to use when executing the onEx function.
   * @return this future.
   */
  default ListenableFuture<T> onException(Consumer<Throwable> onEx, Executor executor) {
    addListener(future -> {
      try {
        future.getUninterruptibly();
        // result is ignored, presumably it is being acquired and acted on elsewhere, e.g. in an onSuccess handler
      } catch (ExecutionException ex) {
        onEx.accept(ex.getCause());
      } catch (Throwable ex) {
        onEx.accept(ex);
      }
    }, executor);
    return this;
  }

  /**
   * The consumer is called when the future completes with an exception condition.
   * <p>
   * NOTE: Uses the {@link MoreExecutors#NON_BLOCKING} executor service.  Therefore, the code should not block.  Provide
   * another executor if a blocking call must be used.
   *
   * @param onEx the consumer that will be called when the future completes with an exception.
   * @return this future.
   */
  default ListenableFuture<T> onException(Consumer<Throwable> onEx) {
    return onException(onEx, MoreExecutors.NON_BLOCKING);
  }

  /**
   * The passed consumer will be called when the future completes successfully with the value.  The consumer's accept
   * function is responsible for handling all exceptions.  Any exceptions thrown from the onSuccess.accept call will
   * be silently ignored.
   *
   * @param onSuccess the consumer to receive the value when it is available.
   * @param executor  the executor to use when executing the onSuccess function.
   * @return this listener.
   */
  default ListenableFuture<T> onSuccess(Consumer<T> onSuccess, Executor executor) {
    addListener(future -> {
      try {
        onSuccess.accept(future.getUninterruptibly());
      } catch (Throwable ex) {
        // ignore. Presumably will be caught by some other method, eg an "onException" handler.
      }
    }, executor);
    return this;
  }

  /**
   * The passed consumer will be called when the future completes successfully with the value.  The consumer's accept
   * function is responsible for handling all exceptions.  Any exceptions thrown from the onSuccess.accept call will
   * be silently ignored.
   * <p>
   * NOTE: Uses the {@link MoreExecutors#NON_BLOCKING} executor service.  Therefore, the code should not block.  Provide
   * another executor if a blocking call must be used.
   *
   * @param onSuccess the consumer to receive the value when it is available.
   * @return this listener.
   */
  default ListenableFuture<T> onSuccess(Consumer<T> onSuccess) {
    return onSuccess(onSuccess, MoreExecutors.NON_BLOCKING);
  }

  /**
   * An alias for {@link #addListener(FutureListener, Executor)} that uses the {@link Consumer} interface.  More for
   * completeness with {@link #onSuccess(Consumer, Executor)}/{@link #onException(Consumer, Executor)} then anything else.
   *
   * @param onCompletion the consumer that will be called when the future is complete.
   * @param executor     the executor to run the completion call on.
   * @return this future.
   */
  default ListenableFuture<T> onCompletion(Consumer<ListenableFuture<T>> onCompletion, Executor executor) {
    addListener(onCompletion::accept, executor);
    return this;
  }

  /**
   * An alias for {@link #addListener(FutureListener)} that uses the {@link Consumer} interface.  More for
   * completeness with {@link #onSuccess(Consumer)}/{@link #onException(Consumer)} then anything else.
   * <p>
   * NOTE: Uses the {@link MoreExecutors#NON_BLOCKING} executor service.  Therefore, the code should not block.  Provide
   * another executor if a blocking call must be used.
   *
   * @param onCompletion the consumer that will be called when the future is complete.
   * @return this future.
   */
  default ListenableFuture<T> onCompletion(Consumer<ListenableFuture<T>> onCompletion) {
    return onCompletion(onCompletion, MoreExecutors.NON_BLOCKING);
  }
}
