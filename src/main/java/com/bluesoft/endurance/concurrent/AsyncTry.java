/*
 * Copyright 2014-2015 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.concurrent;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 *
 * @author Dana P'Simer &lt;danap@bluesoftdev.com&gt;
 */
public class AsyncTry<R, T> {

  private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger( AsyncTry.class );

  private Callable<ListenableFuture<T>> it;
  private Map<Predicate<T>, BiFunction<SettableListenableFuture<R>, T, ListenableFuture<R>>> resultProcessors = new LinkedHashMap<>();
  private Map<Class<? extends Throwable>, BiConsumer<SettableListenableFuture<R>, Throwable>> exCatches = new LinkedHashMap<>();
  private SettableListenableFuture<R> resultFuture = new SettableListenableFuture<>();

  ListenableFuture<R> execute() {
    try {
      final ListenableFuture<T> stepResult = it.call();
      stepResult.addListener( () -> {
        try {
          T result = stepResult.get();
          for ( Map.Entry<Predicate<T>, BiFunction<SettableListenableFuture<R>, T, ListenableFuture<R>>> resultProcessor : resultProcessors.entrySet() ) {
            if ( resultProcessor.getKey().test( result ) ) {
              resultProcessor.getValue().apply( resultFuture, result );
              break;
            }
          }
        } catch ( ExecutionException ex ) {
          Throwable cause = ex.getCause();
          for ( Map.Entry<Class<? extends Throwable>, BiConsumer<SettableListenableFuture<R>, Throwable>> catcher : exCatches.entrySet() ) {
            if ( catcher.getKey().isAssignableFrom( cause.getClass() ) ) {
              catcher.getValue().accept( resultFuture, cause );
              break;
            }
          }
          resultFuture.setException( ex.getCause() );
        } catch ( InterruptedException ex ) {
          resultFuture.setException( ex );
        }
      } );
    } catch ( Throwable t ) {
      resultFuture.setException( t );
    }
    return resultFuture;
  }

  private static final Predicate TRUE_PRED = (t) -> {
    return true;
  };

  private void propagateException( Throwable ex ) {
    if ( !resultFuture.isDone() ) {
      resultFuture.setException( ex );
    }
  }

  public static class Builder<T,R> {

    AsyncTry building = new AsyncTry();
    AsyncTry enclosedBy = null;

    public Builder( Function<T,ListenableFuture<R>> it ) {

    }

    public Builder asyncTry( Function<T, ListenableFuture<R>> it ) {
      Builder subTaskBuilder = new Builder( it );
      subTaskBuilder.enclosedBy = building;
      on( TRUE_PRED, (sf, t) -> {
        final ListenableFuture<R> future = subTaskBuilder.building.execute();
        future.addListener( () -> {
          try {
            R r = future.get();
            sf.set( r );
          } catch ( ExecutionException ex ) {
            sf.setException( ex.getCause() );
          } catch ( InterruptedException ex ) {
            sf.setException( ex );
          }
        } );
        return sf;
      } );
      return subTaskBuilder;
    }

    public AsyncTry build() {
      if ( enclosedBy != null ) {
        // make sure any uncaught exceptions are propagated to the enclosing asyncTry, if they are not already handled.
        if ( !building.exCatches.containsKey( Throwable.class ) ) {
          onException( (SettableListenableFuture<R> sf, Throwable ex) -> {
            enclosedBy.propagateException( ex );
          } );
        }
        return enclosedBy;
      } else {
        return building;
      }
    }

    public <T> Builder on( Predicate<T> matches, BiFunction<SettableListenableFuture<R>, T, ListenableFuture<R>> onMatch ) {
      building.resultProcessors.put( matches, onMatch );
      return this;
    }

    public <T> Builder onException( BiConsumer<SettableListenableFuture<R>, Throwable> onEx ) {
      return onException( Throwable.class, onEx );
    }

    private Builder onException( Class<? extends Throwable> exClass, BiConsumer<SettableListenableFuture<R>, Throwable> onEx ) {
      building.exCatches.put( exClass, onEx );
      return this;
    }
  }
}
