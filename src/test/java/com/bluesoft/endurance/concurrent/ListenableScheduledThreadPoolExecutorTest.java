/*
 * Copyright 2014-2015 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.concurrent;

import org.testng.annotations.Test;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by danap on 9/2/15.
 */
public class ListenableScheduledThreadPoolExecutorTest {

  @Test(enabled = true, timeOut = 2000L)
  public void testListenableScheduledTask() throws Exception {
    long expectedDelay = 500L;
    long started = System.currentTimeMillis();
    ScheduledListenableFuture<Long> future = MoreExecutors.SCHEDULED.schedule(System::currentTimeMillis, expectedDelay, TimeUnit.MILLISECONDS);
    long called = future.get();
    long actualDelay = called - started;

    assert actualDelay - expectedDelay < 2000L : "Actual delay exceeded 2s more then expected: " + actualDelay;
  }

  static class RunnableTimerTask extends TimerTask {
    private Runnable runnable;

    public RunnableTimerTask(Runnable runnable) {
      this.runnable = runnable;
    }

    @Override
    public void run() {
      runnable.run();
    }
  }

  /**
   * Some experiments with the "Timer" class.  Not really testing any code in this library.
   */
  @Test(enabled = false, timeOut = 110L)
  public void testTimer() throws Exception {
    long expectedDelay = 10L;
    long started = System.currentTimeMillis();
    AtomicLong called = new AtomicLong(0);
    AtomicLong called2 = new AtomicLong(0);
    Timer timer = new Timer("test", false);
    timer.schedule(new RunnableTimerTask(() -> called.compareAndSet(0, System.currentTimeMillis())), expectedDelay);
    timer.schedule(new RunnableTimerTask(() -> called2.compareAndSet(0, System.currentTimeMillis())), expectedDelay * 2);

    Thread.sleep(expectedDelay + 100L);
    assert called.get() > 0;
    long actualDelay = called.get() - started;
    assert actualDelay - expectedDelay < 10L : "For first task, actual delay exceeded 100ms more then expected: " + actualDelay;
    Thread.sleep(expectedDelay + 100L);
    assert called2.get() > 0;
    actualDelay = called.get() - started;
    assert actualDelay - expectedDelay * 2 < 10L : "For second task, actual delay exceeded 100ms more then expected: " + actualDelay;
  }
}
