/*
 * Copyright 2014-2015 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.concurrent;

import org.testng.annotations.Test;
import static org.testng.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
/**
 * Created by danap on 9/1/15.
 */
public class FuturesTest {

  private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger( AbstractListenableFutureTest.class );
  private final ListenableExecutorService executor = new ListenableExecutorServiceDecorator( Executors.newFixedThreadPool(2) );
  private static final int ELEMENTS = 20;
  private static final String MESSAGE = "the quick brown fox jumped over the lazy dogs.";

  @Test
  public void testList() throws Exception {
    List<ListenableFuture<String>> futures = new ArrayList<>(ELEMENTS);
    for(int i = 0; i < ELEMENTS; i++) {
      int ii = i;
      futures.add(executor.submit(() -> {
        Thread.sleep(10L);
        return MESSAGE + " " + ii;
      }));
    }

    ListenableFuture<? extends List<String>> futureList = Futures.list(futures,executor);
    List<String> results = futureList.getUninterruptibly();
    assertEquals(results.size(),ELEMENTS);
  }
}
