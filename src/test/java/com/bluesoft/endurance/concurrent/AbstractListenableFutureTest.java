/*
 * Copyright 2014-2015 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.concurrent;

import java.util.concurrent.*;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

/**
 *
 * @author Dana P'Simer &lt;danap@bluesoftdev.com&gt;
 */
public class AbstractListenableFutureTest {

  private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger( AbstractListenableFutureTest.class );
  private final ListenableExecutorService executor = new ListenableExecutorServiceDecorator( Executors.newFixedThreadPool( 1 ) );

  private static final String MESSAGE = "the quick brown fox jumped over the lazy dogs.";

  @Test( enabled = true )
  public void testSet() throws Exception {
    final TestAbstractListenableFuture future = new TestAbstractListenableFuture();
    executor.submit( () -> {
      try {
        Thread.sleep( 100L );
        future.set( MESSAGE );
      } catch ( Throwable ex ) {
        future.setException( ex );
      }
      return (Void)null;
    } );

    String value = future.getUninterruptibly( 10L, TimeUnit.SECONDS );
    assertEquals( value, MESSAGE );
  }

  @Test( enabled = true, timeOut = 10100L )
  public void testSetException() throws Exception {
    final TestAbstractListenableFuture future = new TestAbstractListenableFuture();
    executor.submit(() -> {
      try {
        LOG.info("sleeping for 100ms before setting exception");
        Thread.sleep(100L);
        future.setException(new RuntimeException(MESSAGE));
      } catch (Throwable ex) {
        future.setException(ex);
      }
      return (Void) null;
    });

    try {
      future.getUninterruptibly( 10L, TimeUnit.SECONDS );
      fail( "did not recieve expected exception." );
    } catch ( ExecutionException ex ) {
      assertEquals( ex.getCause().getClass(), RuntimeException.class );
      assertEquals( ex.getCause().getMessage(), MESSAGE );
    }
  }

  @Test( enabled = true, timeOut = 10000L )
  public void testCancel() throws Exception {
    final TestAbstractListenableFuture future = new TestAbstractListenableFuture();
    ListenableFuture<Boolean> taskFuture = executor.submit( () -> {
      try {
        LOG.info( "in testCancel task." );
        Thread.sleep( 100L );
        return future.cancel( true );
      } catch ( Throwable ex ) {
        LOG.error( "Exception while canceling a future.", ex );
        future.setException( ex );
        throw ex;
      }
    } );

    try {
      LOG.info( "waiting for future." );
      future.get( 1L, TimeUnit.SECONDS );
      fail( "did not recieve expected exception." );
    } catch ( InterruptedException | CancellationException ex ) {
      LOG.info( "CancellationException or InterruptedException caught: ", ex );
    }
    LOG.info( "waiting for taskFuture." );
    assertTrue(taskFuture.getUninterruptibly(1L, TimeUnit.SECONDS));
  }

  @Test
  public void testMap() throws Exception {
    final TestAbstractListenableFuture testFuture = new TestAbstractListenableFuture();
    final ListenableFuture<String> future = testFuture.map(s -> s + "Foo!");
    executor.submit(() -> {
      try {
        Thread.sleep(100L);
        testFuture.set(MESSAGE);
      } catch (Throwable ex) {
        testFuture.setException(ex);
      }
      return (Void) null;
    });

    String value = future.getUninterruptibly( 10L, TimeUnit.SECONDS );
    assertEquals(value, MESSAGE + "Foo!");
  }

  @Test( enabled = true, timeOut = 10100L )
  public void testMapWithException() throws Exception {
    final TestAbstractListenableFuture testFuture = new TestAbstractListenableFuture();
    final ListenableFuture<String> future = testFuture.map(s -> s + "Foo!");
    executor.submit(() -> {
      try {
        LOG.info("sleeping for 100ms before setting exception");
        Thread.sleep(100L);
        testFuture.setException(new RuntimeException(MESSAGE));
      } catch (Throwable ex) {
        testFuture.setException(ex);
      }
      return (Void) null;
    });

    try {
      future.getUninterruptibly( 10L, TimeUnit.SECONDS );
      fail( "did not receive expected exception." );
    } catch ( ExecutionException ex ) {
      assertEquals( ex.getCause().getClass(), RuntimeException.class );
      assertEquals( ex.getCause().getMessage(), MESSAGE );
    }
  }

  @Test
  public void testFlatMap() throws Exception {
    final TestAbstractListenableFuture testFuture = new TestAbstractListenableFuture();
    final ListenableFuture<String> future = testFuture.flatMap(s -> executor.submit(() -> {
      Thread.sleep(100L);
      return s + "Foo!";
    }));
    executor.submit(() -> {
      try {
        Thread.sleep(100L);
        testFuture.set(MESSAGE);
      } catch (Throwable ex) {
        testFuture.setException(ex);
      }
      return (Void) null;
    });

    String value = future.getUninterruptibly(10L, TimeUnit.SECONDS);
    assertEquals(value, MESSAGE + "Foo!");
  }

  @Test( enabled = true, timeOut = 10100L )
  public void testFlatMapWithException() throws Exception {
    final TestAbstractListenableFuture testFuture = new TestAbstractListenableFuture();
    final ListenableFuture<String> future = testFuture.flatMap(s -> executor.submit(() -> {
      Thread.sleep(100L);
      return s + "Foo!";
    }));
    executor.submit(() -> {
      try {
        LOG.info("sleeping for 100ms before setting exception");
        Thread.sleep(100L);
        testFuture.setException(new RuntimeException(MESSAGE));
      } catch (Throwable ex) {
        testFuture.setException(ex);
      }
      return (Void) null;
    });

    try {
      future.getUninterruptibly( 10L, TimeUnit.SECONDS );
      fail( "did not receive expected exception." );
    } catch ( ExecutionException ex ) {
      assertEquals( ex.getCause().getClass(), RuntimeException.class );
      assertEquals( ex.getCause().getMessage(), MESSAGE );
    }
  }

  @Test(enabled=true)
  public void testTimeout() {
    final TestAbstractListenableFuture future = new TestAbstractListenableFuture();
    executor.submit(() -> {
      try {
        LOG.info("sleeping for 100ms before setting exception");
        Thread.sleep(100L);
        future.set(MESSAGE);
      } catch (Throwable ex) {
        future.setException(ex);
      }
      return (Void) null;
    });

    try {
      future.timeout(10L,TimeUnit.MILLISECONDS,t ->{},e->{},f -> {
        LOG.info("timeout occurred");
        return true;
      }).getUninterruptibly();
      fail("did not receive expected exception.");
    } catch ( ExecutionException ex ) {
      assertEquals( ex.getCause().getClass(), TimeoutException.class );
    }
  }

  private class TestAbstractListenableFuture extends AbstractListenableFuture<String> {

    public TestAbstractListenableFuture() {
      running();
    }

    public void set( String value ) {
      success( value );
    }

    public void setException( Throwable t ) {
      failed( t );
    }

    @Override
    public boolean cancel( boolean mayInterruptIfRunning ) {
      if ( !isCancelled() ) {
        super.cancelled( mayInterruptIfRunning );
        return true;
      }
      return false;
    }
  }
}
